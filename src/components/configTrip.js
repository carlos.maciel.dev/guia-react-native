import React, { Component } from 'react';
import { Container, Header, Content, Input, Item } from 'native-base';
import { StyleSheet} from 'react-native';


const styles = StyleSheet.create({
    Input: {
        marginTop: 10 
    },
  });
function ConfigTrip(){
  
    return (
      <Container>
       
        <Content>
          <Item style={styles.Input} rounded >
            <Input  placeholder='Local de Saida' />
          </Item>

          <Item style={styles.Input} rounded >
            <Input  placeholder='Local de Chegada' />
          </Item>
        </Content>
      </Container>
    );
  }

  export default ConfigTrip;