
import { StyleSheet} from 'react-native';
import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right, ActionSheet } from 'native-base';

const dataArray = [
    { title: "First Element", content: "Lorem ipsum dolor sit amet" },
    { title: "Second Element", content: "Lorem ipsum dolor sit amet" },
    { title: "Third Element", content: "Lorem ipsum dolor sit amet" }
  ];

  const styles = StyleSheet.create({
    button: {
        marginLeft: 30 
    },
  });

  var numbers = [
    {id: 1, name: "Roteiro 1",image:'https://facebook.github.io/react/logo-og.png' },
    {id: 2, name: "Roteiro 2",image:'https://www.guiadasemana.com.br/contentFiles/system/pictures/2014/4/111933/original/palacio-rio-negro.jpg'},
    {id: 3, name: "Roteiro 3",image:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTGl_n2EuvexpshNn3EwgnER4yPzpRC3nq1ecDYkQxoufoTxdDk'}
  ];

function Home({navigation}){
      return (
      <Container>
    
      <Content >
      {
          numbers.map((el, index) =>
        <Card key={index}>
            <CardItem >
            <Left>
              
              <Body>
                <Text>{el.name}</Text>
                
              </Body>
            </Left>
          </CardItem>
          <CardItem cardBody>
            <Image source={{uri: el.image}} style={{height: 200, width: null, flex: 1}}/>
          </CardItem>
          <CardItem>
          
            <Right>
            <Button onPress= {() => navigation.navigate('ConfigTrip')} dark><Text> Dark </Text></Button>
            </Right>
          </CardItem>
        
        </Card>
          )
        }
      </Content>
    </Container>
    );
  }

export default Home;