import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './components/home'
import ConfigTrip from './components/configTrip'


const Stack = createStackNavigator();

function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Roteiros">
         <Stack.Screen name="Roteiros" component={Home} />
         <Stack.Screen name="ConfigTrip" component={ConfigTrip}  options={{ title: 'Configuração da Viagem' }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;
